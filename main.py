from sys import argv
import os

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression

import utils.getdata as gd
import utils.visuals as v
import utils.ml as ml

# multiclass classification algorithm to predict the flower species based on 
# sepal/petal length/width


def main():
    # setup
    sns.set()
    data = datasets.load_iris()

    print("\n\n=== CSV DATASET ===\n")
    df = gd.importcsv(argv[1])
    print(df)

    # descriptive statistics
    print(df.describe())

    # visual distributions
    columns = ["sepallength", "sepalwidth", "petallength", "petalwidth"]
    v.visual_dist(columns, df)

    # Explorative Data analysis (EDA)
    df["target_name"] = df["target"].map(
            {0: "setosa", 1: "versicolor", 2: "virginica"})

    v.eda(columns, "target", "target_name", df)

    # train-test split (get random parts of the training data for testing)
    ##NOTE: you always wanna evaluate your final model based on test data that
    ##      hasn't been used for training (caveat: cross-validation)

    train_data, test_data = train_test_split(df, test_size=0.25)


    # prepare data for modeling (spittem back out as numpy arrays)
    x_train = train_data.drop(columns=["target", "target_name"]).values
    y_train = train_data["target"].values


    manual_y_pred = np.array(
            [ml.single_feature_prediction(val) for val in x_train[:, 2]])

    man_accuracy = np.mean(manual_y_pred == y_train)

    # create test- and validation-data
    xt, xv, yt, yv = train_test_split(x_train, y_train, test_size=0.25)

    ##INFO: Never evaluate the model on the training data!!!

    y_pred = ml.logistic_regression_prediction(xt, yt, xv)

    # evaluate using validation-set (*v)
    train_acc = np.mean(y_pred == yv)

    # model.score(xv, yv) does the same thing basically

    # Evaluate using cross-validation
    # ml.cross_validation()
    ann_acc = ml.artificial_neural_network_pred(df)


    # Print all results at the end for comparison
    print(f"Manual Accuracy: {man_accuracy * 100:.2f} %")
    print(f"Logistic Regression Training Accuracy: {train_acc * 100:.2f} %")
    print(f"Artificial neural network validation accuracy: {ann_acc * 100:.2f} %")

if __name__ == "__main__":
    main()

