from sys import argv
import os

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

# Visual Distributions
def visual_dist(cols, df):
    os.mkdir("plots")
    for col in cols:
        df[col].hist()
        plt.suptitle(col)
        plt.savefig('plots/' + col + '.png')
        plt.cla()

# Explorative Data Analysis
def eda(cols, tgt, tgt_lbl, df):
    for col in cols:
        sns.relplot(x=col, y=tgt, hue=tgt_lbl, data=df)
        plt.suptitle(col, y=1.05)
        plt.savefig('plots/relplot_' + col + '.png')

    # Summary
    sns.pairplot(df, hue=tgt_lbl)
    plt.savefig('plots/distributions.png')
