import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from tensorflow import keras

# BASELINE modeling

## simplest model in this case is randomly guessing. Since the 3 different
## species are evenly balanced, we expect the accuracy to be at least 33%

# simple manual model
def single_feature_prediction(petal_length):
    """predicts the species based on petal_length alone"""

    if petal_length < 2.5:
        return 0
    elif petal_length < 4.8:
        return 1
    else:
        return 2

def logistic_regression_prediction(xt, yt, xv):
    mod = LogisticRegression(max_iter=200)
    mod.fit(xt, yt)
    return mod.predict(xv)

def cross_validation():
    raise NotImplementedError()

def artificial_neural_network_pred(df):
    ############################################################################
    # If you wanna learn more about this, here's the resource I found the most
    # understandable: 
    #           https://lnwatson.co.uk/posts/intro_to_nn/
    ############################################################################


    # Input is called features, output is called categories
    
    # format dataframe
    iris_data = df.drop(columns=['target'])
    labels = iris_data.pop('target_name')

    # one-hot encoding
    labels = pd.get_dummies(labels) 

    # split data into features and categories as numpy arrays
    X = iris_data.values
    y = labels.values
    
    # Split data into training-, validation-, and test-data
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.05)
    X_train, X_val, y_train, y_val = train_test_split(X_train, y_train, 
                                                      test_size=0.2)

    # Model creation
    m = keras.Sequential()

    # first input layer with size 4 (1 per feature)
    # first hidden layer with size 16
    m.add(keras.layers.Dense(16, activation='relu', input_shape=(4,)))
    # relu = rectified linear unit, creates a non-linear output

    # second and last hidden layer has output
    m.add(keras.layers.Dense(3, activation='softmax'))
    # softmax creates a probability output, so the model can assign a 
    # probability to each possible class

    m.summary()
    input("Press Enter to continue...")

    # compile model
    m.compile(optimizer='adam', loss='categorical_crossentropy', 
              metrics=['accuracy'])
    # optimizer controls the learning rate and updates the models weights+biases
    # loss function determines how well the model performs based on the 
    # difference between the predicted and actual values

    # train model
    m.fit(X_train, y_train, batch_size=12, 
          epochs=200, validation_data=(X_val, y_val))
    # batch size = number of samples to process before updating the model's 
    # weights and biases
    # epochs = the no of times the model goes over the entire training dataset

    # evaluate the model with the training data
    _, acc = m.evaluate(X_test, y_test)
    return acc



