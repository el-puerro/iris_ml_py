import os

import pandas as pd

def importcsv(filename):
    return pd.read_csv(filename)
